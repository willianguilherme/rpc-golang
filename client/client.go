package main

import (
	"fmt"
	"log"
	"net/rpc"
)

// rpc client

type Args struct {
	NumeroUm   float64
	NumeroDois float64
}

func main() {
	hostname := "localhost"
	port := ":1122"

	client, err := rpc.DialHTTP("tcp", hostname+port)
	if err != nil {
		log.Fatal("dialing: ", err)
	} else {
		loop := true
		for loop {
			args := Args{0, 0}
			var total float64

			fmt.Println("\n\nDigite o primeiro número:")
			fmt.Scanln(&args.NumeroUm)

			fmt.Println("\nDigite o segundo número:")
			fmt.Scanln(&args.NumeroDois)

			fmt.Println("\nSelecione uma opcao: \n1 - somar\n2 - subtrair\n3 - multiplicar\n4 - dividir\n9 - sair")

			operacao := ""
			option := 0
			fmt.Scanln(&option)
			fmt.Println("\nVoce selecionou ----- ", option)

			switch option {
			case 1:
				operacao = "soma"
				err = client.Call("Funcoes.Somar", args, &total)
			case 2:
				operacao = "subtração"
				err = client.Call("Funcoes.Subtrair", args, &total)
			case 3:
				operacao = "multiplicação"
				err = client.Call("Funcoes.Multiplicar", args, &total)
			case 4:
				operacao = "divisão"
				err = client.Call("Funcoes.Dividir", args, &total)
			case 9:
				loop = false
				fmt.Println("Saindo...")
			default:
				fmt.Println("***** Opcao invalida -- Tente novamente *****")
			}

			if loop {
				if err != nil {
					log.Fatal("\nerror -- ", err)
				} else {
					log.Printf("\nValores: %f, %f\nResultado da %s: %f\n", args.NumeroUm, args.NumeroDois, operacao, total)
					operacao = ""
				}
			}
		}
	}
}
