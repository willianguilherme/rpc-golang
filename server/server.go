package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type Args struct {
	NumeroUm   float64
	NumeroDois float64
}

type Funcoes string

func (t *Funcoes) Somar(args *Args, total *float64) error {
	*total = args.NumeroUm + args.NumeroDois
	log.Printf("Somar: %f + %f = %f", args.NumeroUm, args.NumeroDois, *total)
	return nil
}

func (t *Funcoes) Subtrair(args *Args, total *float64) error {
	*total = args.NumeroUm - args.NumeroDois
	log.Printf("Subtrair: %f - %f = %f", args.NumeroUm, args.NumeroDois, *total)
	return nil
}

func (t *Funcoes) Dividir(args *Args, total *float64) error {
	*total = args.NumeroUm / args.NumeroDois
	log.Printf("Dividir: %f / %f = %f", args.NumeroUm, args.NumeroDois, *total)
	return nil
}

func (t *Funcoes) Multiplicar(args *Args, total *float64) error {
	*total = args.NumeroUm * args.NumeroDois
	log.Printf("Multiplicar: %f * %f = %f", args.NumeroUm, args.NumeroDois, *total)
	return nil
}

func main() {
	operacoes := new(Funcoes)
	rpc.Register(operacoes)
	rpc.HandleHTTP()

	port := ":1122"

	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal("listen error: ", err)
	} else {
		log.Printf("Server is running in port%s", port)
	}

	http.Serve(listener, nil)
}
