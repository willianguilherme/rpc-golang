# Rpc Golang

Trabalho de sistemas distribuídos
RPC em Golang

## Getting started

Para iniciar basta rodar o cliente e o servidor, para isso é necessário ter o Golang instaldo no computador

# Server
Para rodar o server basta entrar na pasta "server" e rodar no terminal "go run server.go"

# Client
Para rodar o client basta entrar na pasta "client" e rodar no terminal "go run client.go"